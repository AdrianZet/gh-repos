import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Repository, Branch } from '../gh.service';

@Component({
    selector: 'app-repository-item',
    templateUrl: './repository-item.component.html',
    styleUrls: ['./repository-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RepositoryItemComponent {
    @Input() public repository: Repository;
    @Input() public branches: Branch;

    constructor() { }
}
