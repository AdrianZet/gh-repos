import { TestBed } from "@angular/core/testing"
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing"

import { GhService, Repository, Branch } from './gh.service';

describe('GhService', () => {
    let httpTestingControler: HttpTestingController;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                GhService
            ]
        });

        httpTestingControler = TestBed.get(HttpTestingController);
        service = TestBed.get(GhService);
    });

    afterEach(() => {
        httpTestingControler.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('getRepositories', () => {
        let expectedResponse: any[];
        let user: string;
        let baseUrl: string;
        let url: string;

        beforeEach(() => {
            expectedResponse = [
                { name: 'Mockname1', owner: { login: 'User1' } },
                { name: 'Mockname2', owner: { login: 'User2' } }
            ];
            user = 'AdrianZablocki';
            baseUrl = service.baseApiUrl;
            url = `${baseUrl}/users/${user}/repos`;
        });

        it('should call http method with the correct URL', (done: DoneFn) => {
            service.getRepositories(user).subscribe();

            const req = httpTestingControler.expectOne(url);

            expect(req.request.url).toBe(url);
            done();

        });

        it('should call GET method', (done: DoneFn) => {
            service.getRepositories(user).subscribe();

            const req = httpTestingControler.expectOne(url);

            expect(req.request.method).toEqual('GET');
            done();
        });

        it('returned Observable should match the right data', (done: DoneFn) => {
            service.getRepositories(user).subscribe((repos: Repository[]) => {
                expect(repos[0].name).toEqual(expectedResponse[0].name);
                expect(repos[0].login).toEqual(expectedResponse[0].owner.login);
                expect(repos[1].name).toEqual(expectedResponse[1].name);
                expect(repos[1].login).toEqual(expectedResponse[1].owner.login);
                expect(repos.length).toBe(2)
            });

            const req = httpTestingControler.expectOne(url);

            req.flush(expectedResponse);
            done();
        });
    });

    describe('getRepositoryBranches', () => {
        let expectedResponse: any[];
        let user: string;
        let repo: string;
        let baseUrl: string;
        let url: string;

        beforeEach(() => {
            expectedResponse = [
                {
                    name: 'master',
                    commit: {
                        sha: 'ASDO20224ENKLNJDNQJ0I2023OJENJQWDNW0I2342REWQEQSADW32'
                    }
                },
                {
                    name: 'feature-branch',
                    commit: {
                        sha: 'ASDO20224ENKLNJDNQJFGJHLJHHYFGJKLJL0I2342REWQEQSADW32'
                    }
                }
            ]
            repo = 'repoMock'
            user = 'AdrianZablocki';
            baseUrl = service.baseApiUrl;
            url = `${baseUrl}/repos/${user}/${repo}/branches`;
        });

        it('should call http method with the correct URL', (done: DoneFn) => {
            service.getRepositoryBranches(repo, user).subscribe();

            const req = httpTestingControler.expectOne(url);

            expect(req.request.url).toBe(url);
            done();

        });

        it('should call GET method', (done: DoneFn) => {
            service.getRepositoryBranches(repo, user).subscribe();

            const req = httpTestingControler.expectOne(url);

            expect(req.request.method).toEqual('GET');
            done();
        });

        it('should return list of branches', (done: DoneFn) => {
            service.getRepositoryBranches(repo, user).subscribe((branches: Branch[]) => {
                expect(branches[0].name).toEqual(expectedResponse[0].name);
                expect(branches[0].sha).toEqual(expectedResponse[0].commit.sha);
                expect(branches[1].name).toEqual(expectedResponse[1].name);
                expect(branches[1].sha).toEqual(expectedResponse[1].commit.sha);
                expect(branches.length).toBe(2)
            });

            const req = httpTestingControler.expectOne(url);

            req.flush(expectedResponse);
            done();
        });

    });
});