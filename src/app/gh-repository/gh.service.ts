import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

export interface Repository {
    name: string;
    login: string;
}

export interface Branch {
    name: string;
    sha: string;
}

@Injectable()
export class GhService {
    public baseApiUrl: string = 'https://api.github.com';

    private token: string = 'ac1383969a28fc3eb2b2cd779a89287a20423f46';
    private headers = new HttpHeaders().set('authorization', `token ${this.token}`);

    constructor(private http: HttpClient) { }

    public getRepositories(userName: string): Observable<Repository[]> {
        return this.http.get(`${this.baseApiUrl}/users/${userName}/repos`, {
            headers: this.headers
        }).pipe(
            map((response: any[]) => response.map((repository: any): Repository => ({
                name: repository.name,
                login: repository.owner.login,
            }))),
            catchError(this.handleError)
        )
    }

    public getRepositoryBranches(repoName: string, userName: string): Observable<Branch[]> {
        return this.http.get(`${this.baseApiUrl}/repos/${userName}/${repoName}/branches`, {
            headers: this.headers
        }).pipe(
            map((res: any[]) => res.map((item: any) => ({
                name: item.name,
                sha: item.commit.sha
            }))),
            catchError(this.handleError)
        )
    }

    private handleError(err: ErrorEvent) {
        return throwError(err);
    }
}
