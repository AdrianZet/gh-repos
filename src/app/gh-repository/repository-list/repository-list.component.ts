import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Repository, Branch } from '../gh.service';

@Component({
    selector: 'app-repository-list',
    templateUrl: './repository-list.component.html',
    styleUrls: ['./repository-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RepositoryListComponent {
    @Input() public repositories: Repository[];

    public branches: Branch[];
    public isLoading: boolean = false;
    public isError: boolean = false;
    public errorMessage: string;
}
