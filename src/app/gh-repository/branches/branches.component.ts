import { Component, Input, ChangeDetectorRef } from '@angular/core';

import { Branch, GhService } from '../gh.service';

@Component({
    selector: 'app-branches',
    templateUrl: './branches.component.html',
    styleUrls: ['./branches.component.scss']
})
export class BranchesComponent {
    @Input() name: string;
    @Input() login: string;

    public branches: Branch[];
    public isLoading: boolean = false;
    public isError: boolean = false;
    public errorMessage: string;

    constructor(private ghService: GhService, private changeDetector: ChangeDetectorRef) { }

    public onGetBranches(): void {
        this.isLoading = true;
        this.ghService.getRepositoryBranches(this.name, this.login).subscribe(
            (branches: Branch[]) => this.successHandler(branches),
            (error: ErrorEvent) => this.errorHandler(error)
        )
    }

    private successHandler(branches: Branch[]): void {
        this.isLoading = false;
        this.branches = branches;
        this.changeDetector.markForCheck();
    }

    private errorHandler(error: ErrorEvent): void {
        this.isLoading = false;
        this.errorMessage = error.message;
        this.isError = true;
        this.changeDetector.markForCheck();
    }
}
