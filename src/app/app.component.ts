import { Component } from '@angular/core';

import { GhService, Repository } from './gh-repository/gh.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public repositories: Repository[];
    public isLoading: boolean = false;
    public isError: boolean = false;
    public errorMessage: string;

    constructor(private ghService: GhService) { }

    public getRepositories(userName: string): void {
        this.isLoading = true;
        this.ghService.getRepositories(userName).subscribe(
            (response: Repository[]) => this.successHandler(response),
            (error: ErrorEvent) => this.errorHandler(error)
        );
    }

    public onFormClear(): void {
        this.repositories = null;
    }

    private successHandler(response: Repository[]): void {
        this.isLoading = false;
        this.repositories = response
    }

    private errorHandler(error: ErrorEvent): void {
        this.isLoading = false;
        this.errorMessage = error.message;
        this.isError = true;
    }
}
